﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LiTechAdmin/LiTechAdmin.Master" AutoEventWireup="true" CodeBehind="ManageCategories.aspx.cs" Inherits="LightningTech.LiTechAdmin.ManageCategories" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <div class="page-content">
<div class="page-bar">
<div class="page-title-breadcrumb">
<div class=" pull-left">
<asp:Label ID="lblCategoryTitle" CssClass="page-title" runat="server" Text="Add Category"></asp:Label>
</div>
<ol class="breadcrumb page-breadcrumb pull-right">
<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="/LiTechAdmin/Dashboard">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
</li>
<li><a class="parent-item" href="/LiTechAdmin/ManageCategories">Category Management</a>&nbsp;<i class="fa fa-angle-right"></i>
</li>
    <asp:Label ID="lblAddCategory" CssClass="active" runat="server" Text="Add Category"></asp:Label>
</ol>
</div>
</div>
<div class="row">
<div class="col-md-12 col-sm-12">
<div class="card card-box">
<div class="card-head">
<header>Syllabus Details</header>
<button id = "panel-button" 
class = "mdl-button mdl-js-button mdl-button--icon pull-right" 
data-upgraded = ",MaterialButton">
<i class = "material-icons">more_vert</i>
</button>
<ul class = "mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
data-mdl-for = "panel-button">
<li class = "mdl-menu__item"><i class="material-icons">assistant_photo</i>Action</li>
<li class = "mdl-menu__item"><i class="material-icons">print</i>Another action</li>
<li class = "mdl-menu__item"><i class="material-icons">favorite</i>Something else here</li>
</ul>
</div>


<div class="card-body" id="bar-parent">

<div action="#" id="form_sample_1" class="form-horizontal">
<div class="form-body">
<div class="form-group row">
    <asp:Label ID="lblCategory" CssClass="control-label col-md-3" runat="server" Text="Category"></asp:Label>
<div class="col-md-8">
<div class="row">
<div class="col-md-4">
    <asp:TextBox ID="txtCategoryName" CssClass="form-control" MaxLength="50" placeholder="Enter Category Name" runat="server"></asp:TextBox>
    <asp:RequiredFieldValidator ID="RequiredFieldTopic" runat="server" Display="Dynamic" ControlToValidate="txtCategoryName" ForeColor="Red" ErrorMessage="Category Name is Required" ValidationGroup="Submit"></asp:RequiredFieldValidator>
</div>
<div class="col-sm-6">
    <button id="btnAdd" class="btn btn-success" type="button" onclick="return education_fields();">Add Sub Category</button>
</div>
    </div>
</div>
</div>
<div id="education_fields">
    
</div> 
<div class="form-actions">
<div class="row">
<div class="offset-md-3 col-md-9">
    <asp:Button ID="btnSubmit" CssClass="btn btn-info" runat="server" Text="Submit" OnClick="btnSubmit_Click"  ValidationGroup="Submit" />
    <asp:Button ID="btnClear" CssClass="btn btn-default" runat="server" Text="Clear"  />
    <asp:Button ID="btnCancel" CssClass="btn btn-default" runat="server" Text="Cancel" />
</div>
</div>
</div>
</div>
</div>
</div>
<!-- View Features-->
<div class="table-scrollable table-responsive">
<div id="example4_wrapper" class="dataTables_wrapper container-fluid dt-bootstrap4 no-footer">
<div class="row">
<div class="col-sm-12 col-md-6">
<div class="dataTables_length" id="example4_length">
<label>Show
<select name="example4_length" aria-controls="example4" class="form-control form-control-sm">
<option value="10">10</option>
<option value="25">25</option>
<option value="50">50</option>
<option value="100">100</option>
</select> entries</label>
</div>
</div>
<div class="col-sm-12 col-md-6">
<div id="example4_filter" class="dataTables_filter">
<label>Search:
<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="example4">
</label>
</div>
</div>
</div>
<div class="row">
<div class="col-lg-12 col-md-12 col-sm-12 col-12">
<div class="card card-box">
<div class="card-head">
<header>Syllabus List</header>
<div class="tools">
<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
</div>
</div>
<div class="card-body no-padding height-9">
<div class="table-responsive">
    
<table class="table table-striped table-bordered table-hover table-checkable" id="">
<thead>
<tr>
<th class="center" style="width: 400px;">Course Topics</th>
<th class="center">Course SubTopics</th>

<th class="center">Actions </th>
</tr>
</thead>
<tbody>
    <asp:Repeater ID="CategoryRepeater" runat="server" OnItemDataBound="CategoryRepeater_ItemDataBound">
                                            <ItemTemplate>   
										<tr class="odd gradeX">
											<td class="center"><%# Eval("TopicName") %></td>
											<td class="center"></td>											
											<td class="center">										
            <asp:LinkButton ID="LinkEdit" CssClass="btn btn-primary btn-xs" runat="server" CommandArgument='<%# Bind("Id") %>' CommandName="TopicEdit" OnCommand="Action_Command">																
			<i class="fa fa-pencil"></i>
			</asp:LinkButton>
               <asp:LinkButton ID="LinkDelete" runat="server" CommandArgument='<%# Bind("Id") %>' CommandName="TopicDelete" OnCommand="Action_Command" OnClientClick="if ( !confirm('Are you sure you want to delete this Topic?')) return false;">
                   <div class="btn btn-danger btn-xs">
                       <i class="fa fa-trash-o "></i>
                   </div>
               </asp:LinkButton>
											</td>
										</tr>
                                                <asp:Repeater ID="SubCategory" runat="server">
                                                    <ItemTemplate>
										<tr class="odd gradeX">
											<td class="center">  </td>
											<td class="center"><%# Eval("SubTopicName") %></td>
											
											<td class="center">
                                                <asp:LinkButton ID="LinkEdit1" CssClass="btn btn-primary btn-xs" runat="server" CommandArgument='<%# Bind("Id") %>' CommandName="SubTopicEdit" OnCommand="Action_Command">
                                                    <i class="fa fa-pencil"></i>
                                                </asp:LinkButton>
                                                <asp:LinkButton ID="LinkDelete1" runat="server" CommandArgument='<%# Bind("Id") %>' CommandName="SubTopicDelete" OnCommand="Action_Command" OnClientClick="if ( !confirm('Are you sure you want to delete this SubTopic?')) return false;">
                                                <div class="btn btn-danger btn-xs">
                                                    <i class="fa fa-trash-o "></i>
                                                </div>
                                                </asp:LinkButton>
											</td>
										</tr>
                                                        </ItemTemplate>
                                                </asp:Repeater>
                                        </ItemTemplate>
                                                </asp:Repeater>
</tbody>
</table>
</div>
</div>
</div>
</div>
<!-- End View Features-->
</div>
<div class="row">
<div class="col-sm-12 col-md-5">
<div class="dataTables_info" id="example4_info" role="status" aria-live="polite">
    <asp:Label ID="lblResult" runat="server"></asp:Label></div>
</div>
<div class="col-sm-12 col-md-7">
<div class="dataTables_paginate paging_simple_numbers" id="example4_paginate">
<ul class="pagination">
<li class="paginate_button page-item previous disabled" id="example4_previous"><a href="#" aria-controls="example4" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
<li class="paginate_button page-item active"><a href="#" aria-controls="example4" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
<li class="paginate_button page-item next disabled" id="example4_next"><a href="#" aria-controls="example4" data-dt-idx="2" tabindex="0" class="page-link">Next</a></li>
</ul>
</div>
</div>
</div>
</div>
</div> 					
</div>
</div>
</div>
</div>     
  <script>
var room = 1;
function education_fields() {

room++;
var objTo = document.getElementById('education_fields')
var divtest = document.createElement("div");
divtest.setAttribute("class", "form-group removeclass"+room);
var rdiv = 'removeclass'+room;
divtest.innerHTML = '<div class="form-group row"><label class="control-label col-md-3">Course Sub Category<span class="required"> * </span></label><div class="col-md-5"><div class="row"><div class="col-md-8"><input type="text" class="form-control" maxlength="50" id="Coursesub" name="Coursesub[]" value="" placeholder="Course Subtopic Here"></div><div class="col-md-4"><button class="btn btn-danger" type="button" onclick="remove_education_fields(' + room + ');"><span class="glyphicon glyphicon-minus" aria-hidden="true"></span>Remove</button></div></div></div></div>';

objTo.appendChild(divtest)
}
function remove_education_fields(rid) {
$('.removeclass'+rid).remove();
}
</script>

</asp:Content>
