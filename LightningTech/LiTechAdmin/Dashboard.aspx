﻿<%@ Page Title="" Language="C#" MasterPageFile="~/LiTechAdmin/LiTechAdmin.Master" AutoEventWireup="true" CodeBehind="Dashboard.aspx.cs" Inherits="LightningTech.LiTechAdmin.Dashboard" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<div class="page-content">
<div class="page-bar">
<div class="page-title-breadcrumb">
<div class=" pull-left">
<div class="page-title">Dashboard</div>
</div>
<ol class="breadcrumb page-breadcrumb pull-right">
<li>
<i class="fa fa-home"></i>&nbsp;
<a class="parent-item" href="/LiTechAdmin/Dashboard">Home</a>&nbsp;
<i class="fa fa-angle-right"></i>
</li>
<li class="active">Dashboard</li>
</ol>
</div>
</div>
<!-- start widget -->
<div class="state-overview">
<div class="row">
<div class="col-xl-3 col-md-6 col-12">
<div class="info-box bg-b-green">
<span class="info-box-icon push-bottom">
<i class="material-icons">visibility</i>
</span>
<div class="info-box-content">
<span class="info-box-text">Total Views</span>
<span class="info-box-number">450</span>
<div class="progress">
<div class="progress-bar" style="width: 45%"></div>
</div>
<span class="progress-description">
45% Increase in 28 Days
</span>
</div>
<!-- /.info-box-content -->
</div>
<!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-xl-3 col-md-6 col-12">
<div class="info-box bg-b-yellow">
<span class="info-box-icon push-bottom">
<i class="material-icons">thumb_up</i>
</span>
<div class="info-box-content">
<span class="info-box-text">Total Likes</span>
<span class="info-box-number">55</span>
<div class="progress">
<div class="progress-bar" style="width: 40%"></div>
</div>
<span class="progress-description">
20% Increase in 28 Days
</span>
</div>
<!-- /.info-box-content -->
</div>
<!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-xl-3 col-md-6 col-12">
<div class="info-box bg-b-blue">
<span class="info-box-icon push-bottom">
<i class="material-icons">school</i>
</span>
<div class="info-box-content">
<span class="info-box-text">Total Students</span>
<span class="info-box-number">52</span>
<div class="progress">
<div class="progress-bar" style="width: 85%"></div>
</div>
<span class="progress-description">
85% Increase in 28 Days
</span>
</div>
<!-- /.info-box-content -->
</div>
<!-- /.info-box -->
</div>
<!-- /.col -->
<div class="col-xl-3 col-md-6 col-12">
<div class="info-box bg-b-pink">
<span class="info-box-icon push-bottom">
<i class="material-icons">contacts</i>
</span>
<div class="info-box-content">
<span class="info-box-text">Total Enquiries</span>
<span class="info-box-number">13,921</span>
<span>$</span>
<div class="progress">
<div class="progress-bar" style="width: 50%"></div>
</div>
<span class="progress-description">
50% Increase in 28 Days
</span>
</div>
<!-- /.info-box-content -->
</div>
<!-- /.info-box -->
</div>
<!-- /.col -->
</div>
</div>
<!-- end widget -->
<!-- start course list -->
<div class="row">
<div class="col-lg-3 col-md-6 col-12 col-sm-6">
<div class="blogThumb">
<div class="thumb-center">
<img class="img-responsive" alt="user" src="/LiTechAdmin/img/course/course1.jpg">
</div>
<div class="course-box">
<h4>Today Posts</h4>
<div class="text-muted">
<a class="course-likes m-l-10" href="#">
<i class="fa fa-heart-o"></i> 654</a>
</div>
<p>
<span>
<i class="fa fa-book"></i> Course Count: <asp:Label ID="lblCourse" runat="server"></asp:Label></span>
</p>
<a href="/SdxAdmin/ManageCourses">
<button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-info">View More</button>
</a>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6 col-12 col-sm-6 ">
<div class="blogThumb">
<div class="thumb-center">
<img class="img-responsive" alt="user" src="/LiTechAdmin/img/course/course2.jpg">
</div>
<div class="course-box">
<h4>Events</h4>
<div class="text-muted">
<a class="course-likes m-l-10" href="#">
<i class="fa fa-heart-o"></i> 654</a>
</div>
<p>
<span>
<i class="fa fa-calendar"></i> Event Count: <asp:Label ID="lblEvents" runat="server"></asp:Label></span>
</p>
<a href="/SdxAdmin/ManageEvents">
<button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-info">View More</button>
</a>
</div>
</div>
</div>
<div class="col-lg-3 col-md-6 col-12 col-sm-6">
<div class="blogThumb">
<div class="thumb-center">
<img class="img-responsive" alt="user" src="/LiTechAdmin/img/course/course3.jpg">
</div>
<div class="course-box">
<h4>Articles</h4>
<div class="text-muted">
<a class="course-likes m-l-10" href="#">
<i class="fa fa-heart-o"></i> 654</a>
</div>
<p>
<span>
<i class="fa fa-file-text"></i> Articles Count: <asp:Label ID="lblArticles" runat="server"></asp:Label></span>
</p>
<a href="/SdxAdmin/ManageArticles">
<button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-info">View More</button>
</a>
</div>
</div>
</div>        
<div class="col-lg-3 col-md-6 col-12 col-sm-6">
<div class="blogThumb">
<div class="thumb-center">
<img class="img-responsive" alt="user" src="/LiTechAdmin/img/course/course4.jpg">
</div>
<div class="course-box">
<h4>Students</h4>
<div class="text-muted">
<a class="course-likes m-l-10" href="#">
<i class="fa fa-heart-o"></i> 654</a>
</div>
<p>
<span>
<i class="fa fa-mortar-board"></i> Students Count: 200+</span>
</p>
<a href="#">
<button type="button" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-info">View More</button>
</a>
</div>
</div>
</div>
</div>
<!-- End course list -->
</div>
</asp:Content>
