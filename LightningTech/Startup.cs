﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(LightningTech.Startup))]
namespace LightningTech
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
